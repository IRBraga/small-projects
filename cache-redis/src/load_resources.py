import os.path
import csv
import json
import time
from redis_client import RedisConnector


class LoadResources():

    redis = None

    caminho_relativo = os.path.abspath(os.path.dirname(__file__))

    tempo_inicio = None

    def __init__(self, *args, **kargs):
        self.redis = RedisConnector()

    def clear(self):
        self.redis.clear()

    def __timeit(self, label):
        parcial = time.time() - self.tempo_inicio
        horas, rem = divmod(parcial, 3600)
        minutos, segundos = divmod(rem, 60)
        print("{} finalizado em {:0>2}:{:0>2}:{:05.2f}".format(label, int(horas), int(minutos), segundos))
    
    def load_ceps(self):
        self.tempo_inicio = time.time()
        with open(os.path.join(self.caminho_relativo, '../resources/unidades-federativas.csv'), 'r', newline='', encoding='utf-8') as ufs_file:
            reader = csv.DictReader(ufs_file, delimiter=';')
            for uf_cvs in reader:
                with open(os.path.join(self.caminho_relativo, '../resources/ceps-{}.csv'.format(uf_cvs['SIGLA'].lower())), 'r', newline='', encoding='utf-8') as ceps_file:
                    ceps_reader = csv.DictReader(ceps_file, delimiter=';')
                    for ceps_cvs in ceps_reader:
                        self.redis.add(ceps_cvs['CEP'].replace('-',''),json.dumps(ceps_cvs))
                    
                self.__timeit('CEPs da UF {}'.format(uf_cvs['SIGLA']))
        self.__timeit('CEPs')

    def load_ufs(self):
        self.tempo_inicio = time.time()
        with open(os.path.join(self.caminho_relativo, '../resources/unidades-federativas.csv'), 'r', newline='', encoding='utf-8') as ufs_file:
            reader = csv.DictReader(ufs_file, delimiter=';')
            for uf_cvs in reader:
                self.redis.add(uf_cvs['SIGLA'],uf_cvs['NOME'])
        self.__timeit('UFs')

    def load_municipios(self):
        self.tempo_inicio = time.time()
        with open(os.path.join(self.caminho_relativo, '../resources/municipios.csv'), 'r', newline='', encoding='utf-8') as municipios_file:
            reader = csv.DictReader(municipios_file, delimiter=';')
            for municipio_cvs in reader:
                self.redis.add(municipio_cvs['COD_IBGE'],json.dumps(municipio_cvs))
        self.__timeit('Municípios')

    
if __name__ == "__main__":

    load_resources = LoadResources()

    load_resources.clear()
    load_resources.load_ufs()
    load_resources.load_municipios()
    load_resources.load_ceps()

    pass