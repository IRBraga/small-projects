import os.path
import configparser
import redis
import json


class RedisConnector():

    caminho_relativo = os.path.abspath(os.path.dirname(__file__))
    redis_client = None
    config = None

    def __init__(self, *args, **kargs):
        config_file = configparser.ConfigParser()
        config_file.read(os.path.join(self.caminho_relativo,'redis.ini'))
        self.config = config_file['DEFAULT']

        conn_pool = redis.ConnectionPool(
            host=self.config['host'],
            port=self.config['port'],
            password=self.config['password'],
            encoding=self.config['encoding']
        )

        self.redis_client = redis.StrictRedis(connection_pool=conn_pool)

    def clear(self):
        self.redis_client.flushdb()
    
    def add(self, key, content):
        self.redis_client.set(key,content)

    def add_ttl(self, key, content):
        self.redis_client.set(key,content, ex=self.config['ttl'])

    def get(self,key):
        content = self.redis_client.get(key)
        if content:
            try:
                return json.loads(content)
            except json.decoder.JSONDecodeError:
                return content.decode(self.config['encoding'])
        return content