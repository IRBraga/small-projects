import logging

FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logFormatter = logging.Formatter(FORMAT)

fileHandler = logging.FileHandler("find-duplicates.log", "w+")
fileHandler.setFormatter(logFormatter)

logging.basicConfig(level=logging.INFO, format=FORMAT)

logger = logging.getLogger('find-duplicates')
logger.addHandler(fileHandler)
