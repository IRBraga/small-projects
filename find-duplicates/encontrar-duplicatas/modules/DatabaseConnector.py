import sqlite3
from modules.LogConfig import logger


class Database:
    '''
        Classe responsável por criar/conectar com o banco de dados SQLite;
    '''
    conn = None
    tbl_duplicatas = 'DUPLICATAS'

    def __init__(self, db_sufix_name):
        if db_sufix_name:
            self.conn = sqlite3.connect("duplicatas-{}.db".format(db_sufix_name))
        else:
            self.conn = sqlite3.connect("duplicatas.db")

    def limpar(self):
        cursor = self.conn.cursor()
        cursor.execute("""DROP TABLE IF EXISTS {}""".format(self.tbl_duplicatas))
        cursor.execute("""
            CREATE TABLE {} (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                md5 VARCHAR(250) NOT NULL,
                diretorio VARCHAR(250) NOT NULL,
                nome VARCHAR(250) NOT NULL,
                largura INTEGER,
                altura INTEGER,
                data VARCHAR(250),
                hora VARCHAR(250),
                mime VARCHAR(250),
                tamanho INTEGER
            );
        """.format(self.tbl_duplicatas))
        cursor.close()

    def inserir(self, md5, diretorio, nome, largura, altura, data, hora, mime, tamanho):
        try:
            logger.debug("Inserindo valores(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho): {}, {}, {}, {}, {}, {}, {}, {}, {}."
                .format(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho))

            cursor = self.conn.cursor()
            cursor.execute("""INSERT INTO {}(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho) 
                            VALUES (?,?,?,?,?,?,?,?,?)""".format(self.tbl_duplicatas)
                            ,[md5, diretorio, nome, largura, altura, data, hora, mime, tamanho])
            cursor.close()
            self.conn.commit()
        except sqlite3.IntegrityError as e:
            logger.error("Erro ao inserir valores(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho): {}, {}, {}, {}, {}, {}, {}, {}, {}."
                .format(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho), e)

    def obter_duplicatas(self):
        cursor = self.conn.cursor()
        cursor.execute("""select distinct md5, data from {} order by md5, data""".format(self.tbl_duplicatas))
        linhas = cursor.fetchall()
        cursor.close()
        return linhas
    
    def obter_primeiro_md5(self, md5):
        cursor = self.conn.cursor()
        cursor.execute("""select diretorio, nome from {} where md5 = ?""".format(self.tbl_duplicatas), [md5])
        linhas = cursor.fetchone()
        cursor.close()
        return linhas

    def __exit__(self, *args):
        self.conn.close()

