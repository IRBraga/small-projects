import hashlib
import mimetypes
import os
from PIL import Image, ExifTags
from modules.LogConfig import logger


class FileExifInfo:
    '''
        Classe que obtém informações dos arquivos de imagem;
    '''
    @staticmethod
    def __exif_info(path, mime):

        if mime and mime.startswith("image"):
            try:
                img = Image.open(path)
                exif_data = img._getexif()

                if exif_data:

                    exif = dict((ExifTags.TAGS[k], v) for k, v in exif_data.items() if k in ExifTags.TAGS)

                    altura = exif["ExifImageHeight"]
                    data, hora = exif["DateTime"].split(" ")
                    largura = exif["ExifImageWidth"]

                    return largura, altura, data, hora
                else:
                    logger.debug("Sem dados de Exif...")
                    return None, None, None, None
            except Exception as e:
                logger.error(path)
                logger.error(e)

        return None, None, None, None

    @staticmethod
    def obter_info(path):

        hash_md5 = hashlib.md5()
        diretorio, nome_arquivo = path.rsplit("/", 1)
        mime = mimetypes.MimeTypes().guess_type(path)[0]
        tamanho = os.path.getsize(path)

        logger.info("Caminho: {}, Mime: {}, Tamanho: {}".format(path, mime, tamanho))

        try:
            file = open(path, "rb")

            with file as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    hash_md5.update(chunk)
            file.close()
            largura, altura, data, hora = FileExifInfo.__exif_info(path, mime)
            return hash_md5.hexdigest(), diretorio, nome_arquivo, largura, altura, data, hora, mime, tamanho

        except Exception as e:
            logger.error(e)
            return 0, diretorio, nome_arquivo, None, None, None, None, mime, tamanho

