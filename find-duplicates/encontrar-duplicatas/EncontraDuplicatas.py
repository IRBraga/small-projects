import os
import time
from optparse import OptionParser
from modules.LogConfig import logger
from modules.DatabaseConnector import Database
from modules.FileExifInfo import FileExifInfo


class EncontraDuplicatas:
    '''
        Classe responsável por:
            - Criar o arquivo de banco de dados de saída;
            - Iniciar o processo de busca por arquivos e pastas recursivamente;
    '''
    tempo_inicio = None
    caminho = None
    db = None

    def __init__(self, dir_inicial, sufixo_db):
        self.tempo_inicio = time.time()
        self.caminho = dir_inicial
        self.db = Database(sufixo_db)
        self.db.limpar()

    def iniciar(self):
        self.__escavar(self.caminho)

    def __escavar(self, caminho):
        if os.path.isfile(caminho):
            self.__adicionar(caminho)
        elif os.path.isdir(caminho):
            logger.info("Processing files in {}".format(caminho))
            for root, directories, files in os.walk(caminho):
                for directory in directories:
                    logger.info("Processing files in {}".format(directory))
                for file in files:
                    self.__adicionar(os.path.join(root, file))

    def __adicionar(self, full_caminho_file):
        md5, diretorio, nome, largura, altura, data, hora, mime, tamanho = FileExifInfo.obter_info(full_caminho_file)
        self.db.inserir(md5, diretorio, nome, largura, altura, data, hora, mime, tamanho)

    def __exit__(self, *args):
        tempo_final = time.time() - self.tempo_inicio
        horas, rem = divmod(tempo_final, 3600)
        minutos, segundos = divmod(rem, 60)
        logger.info("Finalizado em {:0>2}:{:0>2}:{:05.2f}".format(int(horas), int(minutos), segundos))


parser = OptionParser()
parser.add_option("-d", "--diretorio", dest="param_diretorio",
                  help="Diretório de início da pesquisa.", metavar="FILE")
parser.add_option("-s", "--sufixo", dest="param_db_sufixo",
                  help="Sufixo para o nome do arquivo de saída.", metavar="FILE")
(options, args) = parser.parse_args()
if __name__ == '__main__':

    if not options.param_diretorio:
        logger.error("Deve indicar ao menos o parâmetro [-d] com a pasta de início!")
    else:
        if options.param_db_sufixo == 'default_value':
            options.param_db_sufixo = None
        encontra_duplicatas = EncontraDuplicatas(options.param_diretorio, options.param_db_sufixo)
        encontra_duplicatas.iniciar()
