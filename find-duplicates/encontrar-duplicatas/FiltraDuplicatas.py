import sqlite3
import shutil
import os
import time
from optparse import OptionParser
from modules.LogConfig import logger
from modules.DatabaseConnector import Database


class FiltraDuplicatas:
    '''
        Classe que move para outro diretório apenas 1 arquivo por duplicata;
    '''
    tempo_inicio = None
    db = None
    diretorio_destino = None

    def __init__(self, diretorio_destino, sufixo):
        self.tempo_inicio = time.time()
        if diretorio_destino.endswith('/'):
            self.diretorio_destino = diretorio_destino
        else:
            self.diretorio_destino = diretorio_destino + '/'
        self.db = Database(sufixo)

    def iniciar(self):
        rows = self.db.obter_duplicatas()

        for row in rows:

            md5, data = row

            arquivo = self.db.obter_primeiro_md5(md5)

            if not data:
                dir_nome_data = 'None/'
            else:
                dir_nome_data = data.replace(':', '') + '/'
            
            dir_arq_db, nome_arq_db = arquivo
            
            caminho_origem_completo = dir_arq_db + '/' + nome_arq_db
            caminho_destino_completo = self.diretorio_destino + dir_nome_data

            if not os.path.exists(caminho_destino_completo):
                os.makedirs(caminho_destino_completo)

            logger.info("Copiando {} -> {}".format(caminho_origem_completo,caminho_destino_completo + nome_arq_db))
            shutil.copyfile(caminho_origem_completo, caminho_destino_completo + nome_arq_db)

    def __exit(self, *args, **kargs):
        tempo_final = time.time() - self.tempo_inicio
        horas, rem = divmod(tempo_final, 3600)
        minutos, segundos = divmod(rem, 60)
        logger.info("Finalizado em {:0>2}:{:0>2}:{:05.2f}".format(int(horas), int(minutos), segundos))

parser = OptionParser()
parser.add_option("-d", "--diretorio", dest="param_diretorio",
                  help="Diretório de início da pesquisa.", metavar="FILE")
parser.add_option("-s", "--sufixo", dest="param_db_sufixo",
                  help="Sufixo para o nome do arquivo de saída.", metavar="FILE")
(options, args) = parser.parse_args()

if __name__ == '__main__':

    if not options.param_diretorio:
        logger.error("Deve indicar ao menos o parâmetro [-d] com a pasta de início!")
    else:
        if options.param_db_sufixo == 'default_value':
            options.param_db_sufixo = None
        filtra_duplicatas = FiltraDuplicatas(options.param_diretorio, options.param_db_sufixo)
        filtra_duplicatas.iniciar()
