from setuptools import find_packages, setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="encontrar-duplicatas",
    version="0.0.1",
    author="Igor Ribeiro Braga",
    author_email="igor.braga@outlook.com",
    description="Projeto com intuito de identificar arquivos duplicados.",
    long_description=long_description,
    url="https://gitlab.com/IRBraga/small-projects/",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)