# Find-Duplicates

## Código Python para encontrar e mover arquivos duplicados pelo MD5

Este código se propõe a identificar duplicatas de arquivos calculando seu MD5 e armazenando algumas informações encontradas sobre eles em um arquivo SQLite. São elas:

* MD5
* Diretório onde se encontra
* Nome
* Largura (Caso seja uma imagem)
* Altura (Caso seja uma imagem)
* Mime Type (Caso seja uma imagem)
* Data de criação
* Hora de criação
* Tamanho do arquivo em bytes

A criação do arquivo `duplicatas.db` serve para avaliar se o programa faz o que deveria e serve também como entrada para outro programa que filtra os arquivos encontrados e os copia para outra pasta sem considerar suas cópias.

Ao se mover esses arquivos o seguinte padrão é utilizado:

1. Obtêm-se a Data de criação (YYYYMMDD) do arquivo obtida para criar-se uma pasta com esse nome afim de manter a pasta destino mais organizada possível;
2. Caso o arquivo não tenha essa informação, a palavra `None` é utilizada.;
3. Dentro destas pastas os arquivos manterão os nomes obtidos na busca inicial.

## Uso via Makefile

Os seguintes comandos são suportados:

Criar a pasta de virtualenv do python:
```
make venv
```
Ativar o virtualenv:
```
make ativar
```
Baixar as dependências para execução:
```
make build
```
Encontrar as duplicatas:
```
make encontrar dir=(obrigatorio) sufixo=(opcional)
```
Aqui é obrigatório informar a pasta inicial onde será feita a busca. O outro parâmetro de `sufixo` deve ser informado para que o arquivo de saída (SQLite) tenha um nome diferente do padrão.

Filtrar as duplicatas:
```
make filtrar dir=(obrigatorio) sufixo=(opcional)
```
Aqui é obrigatório informar a pasta onde serão armazenados os arquivos. O outro parâmetro de `sufixo` deve ser informado para que o arquivo de entrada (SQLite) tenha um nome diferente do padrão.

Empacotar para distribuição com as fontes:
```
make empacotar-fontes
```

Empacotar para distribuição:
```
make empacotar
```

Limpar as pastas de build e empacotamento:
```
make limpar
```