#!/usr/bin/python
# -*- coding: utf-8 -*-
from setuptools import find_packages
from distutils.core import setup

setup(
   name='NameBuilder',
   version='0.0.1',
   description='Módulo para criar massa com dados de pessoas fictícias.',
   author='Igor Ribeiro Braga',
   author_email='igor.braga@outlook.com',
   packages=find_packages(),
   include_package_data=True,
   license='GNU',
   install_requires=[
      'numpy',
      'pandas',
      'requests',
      'urllib3',
      'certifi',
      'chardet',
      'idna',
      'python-dateutil',
      'pytz',
      'six',
      'wheel',
      'setuptools'
   ],
   
)