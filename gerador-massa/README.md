# Name Builder

O propósito deste código é gerar dados de pessoas físicas fictícias para poder estudar as informações geradas aleatoriamente com estatística para montagem de plots e extração de informação de uma amostra e/ou população.

Os dados são armazenados em um arquivo no formato CSV e em um banco de dados SQLite.

## Dados gerados
---
As pessoas geradas possuem nome (simples ou composto), até 5 sobrenomes e idade que varia dos 14 aos 75 anos.

### Regras

#### Nomes

Os nomes das pessoas podem ser simples ou compostos. A escolha disse é aleatória com chances iguais de 50%, da mesma forma como se o nome será masculino ou feminino. 

Os arquivos que possuem listas de nomes estão na pasta ``NameBuilder/resources``, são eles:

1. compostos-femininos.txt
1. compostos-masculinos.txt
1. simples-femininos.txt
1. simples-masculinos.txt

#### Sobrenomes

A quantidade de sobrenomes são escolhidos de forma aleatória com diferença de probabilidades:

1. 15% de ter apenas 1 sobrenome
1. 40% de ter 2 sobrenomes
1. 25% de ter 3 sobrenomes
1. 15% de ter 4 sobrenomes
1. 5% de ter 5 sobrenomes

Os valores aplicados acima foram definidos por mim, sem nanhum tipo de estudo aplicado.

Após a definição da quantidade de sobrenomes, a escolha deles é feita de forma também aleatória, mas com probabilidades iguais de serem escolhidos.

O arquivo que possue lista de sobrenomes está na pasta ``NameBuilder/resources/sobrenomes.txt``.

#### CPF

O doucmento de CPF é criado para todos os registros. O código que uso aqui é do Felipe Lessa e pode ser encontrado [nesse link.](https://wiki.python.org.br/GeradorDeCpf)

**Obs.**: Não é verificado em tempo de execução se um mesmo CPF foi utilizado por mais de uma pessoa.

#### CEP

Os CEPs utilizados foram obtidos no site do [República Virtual](http://www.republicavirtual.com.br/cep/index.php). Os dados de CEPs são de **domínio público** mas ainda são de difícil acesso.

Para diminuir o overhead os CEPs foram quebrados em vários outros por unidade federativa e estão na pasta ``NameBuilder/resources/ceps-<UF>.csv``.

**Obs.**: Não é verificado em tempo de execução se um mesmo CEP foi utilizado por mais de uma pessoa.

## Dependências
---
O projeto foi desenvolvido usando o Python 3.6.5. As dependências estão descritas no arquivo ``requirements.txt`` no root do projeto.

Para a instalação das dependências, deve-se ter instalado o pip. Caso não tenha o pip instalado, instale-o:

> sudo apt-get install python-pip python3-pip

## Execução
---
Para executar o código deve-se baixar as dependências e rodar o script Python.

Na pasta do projeto, crie o ambiente virtual do Python:

> python3 -m venv venv

Agora, ative-o:

> . venv/bin/activate

Instale as depedências do projeto via pip:

> python -m pip install -r requirements.txt

Para executar a aplicação, estando no root do projeto:

> python NameBuilder/main.py

A aplicação aceita passagem de parâmetros. São eles:

``-t ou --total``: Aceita um valor inteiro positivo para indicar a quantidade de registros a ser gerado.

``-n ou --nome``: String para indicar o nome do arquivo de saída csv e banco de dados SQLite.

``-u ou --unidadefederativa``: String para indicar a unidade federativa que o endereço será utilizado para a pessoa fictícia.

``-m ou --municipio``: String para indicar o município de endereço da pessoa fictícia. Essa opção ignora o parâmetro -u, pois a informação do município prevalece quanto a unidade federativa.

Exemplo:
> python NameBuilder/main.py -t 150 -n NOVA_MASSA

A saída disso será um arquivo chamado ``NOVA_MASSA.csv`` e um banco de dados chamado ``NOVA_MASSA.db`` que contém 150 registros.