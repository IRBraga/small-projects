 #!/usr/bin/python
 # -*- coding: utf-8 -*-
import time
from optparse import OptionParser
from NameBuilder import NameBuilder
from CsvBuilder import CsvBuilder
from CpfBuilder import CpfBuilder
from CepFactory import CepFactory
from NaturalidadeProvider import NaturalidadeProvider

parser = OptionParser()
parser.add_option('-t', '--total', dest='param_total_registros',
                  help='Total de registros a serem criados.', metavar='FILE')
parser.add_option('-n', '--nome', dest='param_nome_arquivo_saida',
                  help='Nome do arquivo de saída.', metavar='FILE')
parser.add_option('-u', '--unidadefederativa', dest='param_uf',
                  help='Define uma única unidade federativa para a massa.', metavar='FILE')
(options, args) = parser.parse_args()

def main():

    t_app_startup = time.time()
    
    qtd_registros = 1
    uf = None

    if options.param_total_registros is not None:
        qtd_registros = int(options.param_total_registros)

    if options.param_uf is not None:
        uf = options.param_uf

    name_builder = NameBuilder()
    cpf_builder = CpfBuilder()
    cep_factory = CepFactory()
    naturalidade_provider = NaturalidadeProvider()

    if not options.param_nome_arquivo_saida:    
        csv_builder = CsvBuilder()
    else:
        csv_builder = CsvBuilder(options.param_nome_arquivo_saida)

    for count in range(qtd_registros):

        nome, sobrenome = name_builder.gerar_nome_completo()
        idade = name_builder.definir_idade()
        cpf = cpf_builder.gera_cpf()
        municipio, logradouro, bairro, cep, tipo_logradouro, uf_endereco = cep_factory.get(uf).get_random()
        uf_naturalidade, municipio_naturalidade, cod_ibge_naturalidade = naturalidade_provider.get_random()

        csv_builder.escrever(nome, sobrenome, idade, uf_naturalidade, municipio_naturalidade, cpf, municipio, logradouro, bairro, cep, tipo_logradouro, uf_endereco)

        count += 1

        print('{} registro(s).'.format(count))

    t_app_ended = time.time() - t_app_startup

    hours, rem = divmod(t_app_ended, 3600)
    minutes, seconds = divmod(rem, 60)
    print('Finalizado em {:0>2}:{:0>2}:{:05.2f}'.format(int(hours), int(minutes), seconds))

if __name__ == '__main__':
    main()
    pass