#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv, os.path
import pandas as pd


class UfProvider(object):
    '''
        Classe que retorna dados sobre Unidades Federativas.
    '''
    caminho_relativo = os.path.abspath(os.path.dirname(__file__))

    def __init__(self, *args, **kwargs):
        self.filename = 'unidades-federativas.csv'
        self.source = pd.read_csv(os.path.join(self.caminho_relativo, 'resources/{}'.format(self.filename)), encoding='utf8', sep=';')
        return super().__init__(*args, **kwargs)
    
    def get_random(self):
        '''
            Método que seleciona um Dataframe Sample e retorna os valores como uma lista.
        '''
        dataframe = self.source.sample(n=1)
        return dataframe.values.tolist()[0]

    def get(self, linha):
        return self.source.iloc[linha].values.tolist()

    def get_ufs(self):
        return self.source['SIGLA'].values.tolist()
