import logging

FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logFormatter = logging.Formatter(FORMAT)

fileHandler = logging.FileHandler("NameBuilder.log", "w+")
fileHandler.setFormatter(logFormatter)

logging.basicConfig(level=logging.INFO, format=FORMAT)

logger = logging.getLogger('NameBuilder')
logger.addHandler(fileHandler)
