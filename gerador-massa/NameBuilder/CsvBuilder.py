#!/usr/bin/python
# -*- coding: utf-8 -*-
import csv


class CsvBuilder(object):
    '''
        Classe que cria o arquivo csv da lista de pessoas.
    '''
    def __init__(self, file_name='NameBuilder.csv'):
        if not file_name.endswith('.csv'):
            file_name += '.csv'

        self.csv_file = open(file_name, 'w', newline='', encoding='utf-8')
        colunas = ['NOME','SOBRENOME','IDADE', 'UF_NATURALIDADE', 'MUNICIPIO_NATURALIDADE','CPF','MUNICIPIO','LOGRADOURO','BAIRRO','CEP','TIPO_LOGRADOURO','UF']
        self.writer = csv.DictWriter(self.csv_file, colunas, delimiter=';')
        self.writer.writeheader()

    def __del__(self):
        self.csv_file.close()

    def escrever(self, nome, sobrenome, idade, uf_naturalidade, municipio_naturalidade, cpf, municipio, logradouro, bairro, cep, tipo_logradouro, uf):
        self.writer.writerow({'NOME': nome, 'SOBRENOME': sobrenome, 'IDADE': idade, 'UF_NATURALIDADE': uf_naturalidade, 'MUNICIPIO_NATURALIDADE': municipio_naturalidade, 'CPF': cpf, 'MUNICIPIO': municipio, 'LOGRADOURO': logradouro, 'BAIRRO': bairro, 'CEP': cep, 'TIPO_LOGRADOURO': tipo_logradouro, 'UF': uf})
