#!/usr/bin/python
# -*- coding: utf-8 -*-
from UfProvider import UfProvider
from CepProvider import CepProvider


class CepFactory(object):
    '''
        Factory de Ceps por UF.
    '''
    uf_provider = UfProvider()
    factory_dict = None

    def __init__(self):
        self.factory_dict = dict.fromkeys(self.uf_provider.get_ufs())
    
    def __get_cep_provider(self, uf):

        if self.factory_dict[uf] is None:
            self.factory_dict[uf] = CepProvider(uf)

        return self.factory_dict[uf]

    def get(self, uf=None):

        if uf is None:
            nome_uf, uf = self.uf_provider.get_random()
        else:
            uf = uf.upper()

        return self.__get_cep_provider(uf)


if __name__ == "__main__":

    cep_factory = CepFactory()
    print(cep_factory.get().get_random())
    print(cep_factory.get('sp').get_random())

    pass
