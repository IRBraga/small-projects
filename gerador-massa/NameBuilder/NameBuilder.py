 #!/usr/bin/python
 # -*- coding: utf-8 -*-
import random, os.path
from random import randint
import numpy as np


class NameBuilder(object):
    '''
        Classe utilizada para gerar nomes e idades para pessoas fictícias.
    '''
    def __init__(self):
        # Arquivos com os dados
        caminho_relativo = os.path.abspath(os.path.dirname(__file__))

        self.compostos_femininos = open(os.path.join(caminho_relativo, 'resources/compostos-femininos.txt'), 'r', encoding='utf8')
        self.compostos_masculinos = open(os.path.join(caminho_relativo, 'resources/compostos-masculinos.txt'), 'r', encoding='utf8')
        self.simples_femininos = open(os.path.join(caminho_relativo, 'resources/simples-femininos.txt'), 'r', encoding='utf8')
        self.simples_masculinos = open(os.path.join(caminho_relativo, 'resources/simples-masculinos.txt'), 'r', encoding='utf8')
        self.sobrenomes = open(os.path.join(caminho_relativo, 'resources/sobrenomes.txt'), 'r', encoding='utf8')

    def __del__(self):
        # Fechar os arquivos abertos
        self.compostos_femininos.close()
        self.compostos_masculinos.close()
        self.simples_femininos.close()
        self.simples_masculinos.close()
        self.sobrenomes.close()

    def __escolher_linha_randomica(self, afile):
        '''
            Escolhe uma linha randomica de um arquivo texto.
        '''
        afile.seek(0)
        line = next(afile)
        for num, aline in enumerate(afile, 2):
            if random.randrange(num): continue
            line = aline
        return line.replace('\n','').replace('\r','')

    def gerar_nome(self):
        '''
            Definir se o nome terá nomes composto ou simples e quantos sobrenomes terá (limitado a 5)
        '''

        # Simples (0) ou composto (1)
        # Chances iguais de escolha (randômico)
        if randint(0,1) == 0:
            # Feminino (0) ou Masculino (1)
            if randint(0,1) == 0:
                nome = self.__escolher_linha_randomica(self.simples_femininos)
            else:
                nome = self.__escolher_linha_randomica(self.simples_masculinos)
        else:
            # Feminino (0) ou Masculino (1)
            if randint(0,1) == 0:
                nome = self.__escolher_linha_randomica(self.compostos_femininos)
            else:
                nome = self.__escolher_linha_randomica(self.compostos_masculinos)

        return nome

    def gerar_sobrenome(self):
        '''
            Quantos sobrenomes terá?
                15% de ter apenas 1 sobrenome
                40% de ter apenas 2 sobrenome
                25% de ter apenas 3 sobrenome
                15% de ter apenas 4 sobrenome
                5% de ter apenas 5 sobrenome
        '''
        sobrenome = ''
        qtd_sobrenomes = np.random.choice(a = np.arange(1,6), p= [0.15, 0.40, 0.25, 0.15, 0.05])

        for i in range(qtd_sobrenomes):
            sobrenome += self.__escolher_linha_randomica(self.sobrenomes) + ' '
        
        return sobrenome.rstrip()

    def gerar_nome_completo(self):
        '''
            Gera o nome com sobrenome.
        '''
        return self.gerar_nome(), self.gerar_sobrenome()

    def definir_idade(self):
        '''
            Gera a idade entre os valores de 14 a 75.
        '''
        return randint(14,75)
