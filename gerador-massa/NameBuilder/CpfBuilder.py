 #!/usr/bin/python
 # -*- coding: utf-8 -*-
import random


class CpfBuilder(object):
    '''
        Classe que cria um CPF.
        Créditos para Felipe Lessa (https://wiki.python.org.br/GeradorDeCpf).
    '''
    def gera_cpf(self):
        def calcula_digito(digs):
            s = 0
            qtd = len(digs)
            for i in range(qtd):
                s += n[i] * (1+qtd-i)
            res = 11 - s % 11
            if res >= 10: return 0
            return res                                                                              
        n = [random.randrange(10) for i in range(9)]
        n.append(calcula_digito(n))
        n.append(calcula_digito(n))
        return "%d%d%d.%d%d%d.%d%d%d-%d%d" % tuple(n)
