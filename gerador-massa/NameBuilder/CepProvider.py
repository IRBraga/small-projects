#!/usr/bin/python
# -*- coding: utf-8 -*-
import pandas as pd
import os.path
from random import randint
from UfProvider import UfProvider


class CepProvider(object):
    '''
        Classe responsável por ler um csv de cep e disponibilizar os endereços da UF especificada.
    '''
    caminho_relativo = os.path.abspath(os.path.dirname(__file__))
    uf_provider = UfProvider()

    def __init__(self, uf=None):

        # Se não for passado a UF, escolher aleatoriamente.
        if uf is None:
            uf = self.uf_provider.get_random()[1]

        self.filename = 'ceps-{}.csv'.format(uf.lower())
        self.source = pd.read_csv(os.path.join(self.caminho_relativo, 'resources/{}'.format(self.filename)), encoding='utf8', sep=';')

    def get_random(self):
        '''
            Método que seleciona um Dataframe Sample e retorna os valores como uma lista.
            Município, logradouro, bairro, cep, tipo_logradouro e uf.
        '''
        dataframe = self.source.sample(n=1)
        return dataframe.values.tolist()[0]

    def get(self, linha):
        return self.source.iloc[linha].values.tolist()

    def qtd_linhas(self):
        return len(self.source.index)


if __name__ == '__main__':
    pb = CepProvider()
    municipio, logradouro, bairro, cep, tipo_logradouro, uf = pb.get_random()
    print(bairro)

    municipio, logradouro, bairro, cep, tipo_logradouro, uf = pb.get(0)

    print(cep)
