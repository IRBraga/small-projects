#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import time
import csv, os, os.path
from random import randint
from optparse import OptionParser
from CepProvider import CepProvider
from LogConfig import logger


caminho_relativo = os.path.abspath(os.path.dirname(__file__))

class CepCorrection(object):
    '''
        Classe para acessar atualizar o endereço pelo cep acessando um serviço.
    '''
    url = 'https://api.postmon.com.br/v1/cep/{}'

    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    def __del__(self):
        logger.info('[CepCorrection] Finalizado.')

    def get(self, cep):
        r = requests.get(self.url.format(cep.replace('-','')))
        if r.status_code == 200:
            return r.json()
        else:
            return None

def processa_uf(uf):

    #total_requests = 0

    uf_encontrados = uf + '.csv'
    uf_nao_encontrados = uf + '-nao-encontrados.csv'
    colunas = ['MUNICIPIO','LOGRADOURO','BAIRRO','CEP','UF']

    cep_provider = CepProvider(uf)
    cep_correction = CepCorrection()

    csv_encontrados = open(uf_encontrados, 'w', newline='', encoding='utf-8')
    writer_encontrados = csv.DictWriter(csv_encontrados, colunas, delimiter=';')
    writer_encontrados.writeheader()

    csv_nao_encontrados = open(uf_nao_encontrados, 'w', newline='', encoding='utf-8')
    writer_nao_encontrados = csv.DictWriter(csv_nao_encontrados, ['CEP'], delimiter=';')
    writer_nao_encontrados.writeheader()

    for i in range(cep_provider.qtd_linhas()):
        logger.info('Registro {} de {}'.format(i+1,cep_provider.qtd_linhas()))
        municipio, logradouro, bairro, cep, tipo_logradouro, uf = cep_provider.get(i)
        
        try:
            cep_consultado = cep_correction.get(cep)
        except requests.exceptions.ConnectionError:
            logger.info('(%s) Esperar 5min.' % time.ctime())
            time.sleep(300)
            cep_consultado = cep_correction.get(cep)
        
        logger.info(cep_consultado)

        if cep_consultado is not None:
            try:
                writer_encontrados.writerow({'MUNICIPIO': cep_consultado['cidade'], 'LOGRADOURO': cep_consultado['logradouro'], 'BAIRRO': cep_consultado['bairro'], 'CEP': cep_consultado['cep'], 'UF': cep_consultado['estado']})
            except KeyError:
                logger.info('KeyError -> {}'.format(cep_consultado))
        else:
            writer_nao_encontrados.writerow({'CEP': cep})

    csv_encontrados.close()
    csv_nao_encontrados.close()

    # Move os arquivos para a pasta de completados
    try:
        os.rename(os.path.join(caminho_relativo, uf_encontrados),os.path.join(caminho_relativo, 'corrigidos/{}'.format(uf_encontrados)))
        os.rename(os.path.join(caminho_relativo, uf_nao_encontrados),os.path.join(caminho_relativo, 'corrigidos/{}'.format(uf_nao_encontrados)))
    except FileNotFoundError:
        os.rename(os.path.join(caminho_relativo, '../{}'.format(uf_encontrados)),os.path.join(caminho_relativo, 'corrigidos/{}'.format(uf_encontrados)))
        os.rename(os.path.join(caminho_relativo, '../{}'.format(uf_nao_encontrados)),os.path.join(caminho_relativo, 'corrigidos/{}'.format(uf_nao_encontrados)))
    

parser = OptionParser()
parser.add_option('-i', '--ignore', dest='param_ignore',
                  help='Lista de UFs a serem ignorados.', metavar='FILE')
parser.add_option('-r', '--resume', dest='param_resume', nargs=0, 
                  help='Retoma as consultas de onde parou o arquivo csv.', metavar='FILE')
parser.add_option('-m', '--municipio', dest='param_municipio', nargs=1, type='string',
                  help='Especifica o município para a correção.', metavar='FILE')

(options, args) = parser.parse_args()

if __name__ == '__main__':

    uf_ignorados = None

    if options.param_municipio:
        logger.info(options.param_municipio)
        processa_uf(options.param_municipio.lower())
    else:
        if options.param_ignore:
            uf_ignorados = options.param_ignore.split(',')

        with open(os.path.join(caminho_relativo, 'resources/unidades-federativas.csv'), 'r', newline='', encoding='utf-8') as ufs_file:
            reader = csv.DictReader(ufs_file, delimiter=';')
            for uf_cvs in reader:
                nome_uf = uf_cvs['SIGLA'].lower()

                if not uf_ignorados or nome_uf not in uf_ignorados:                
                    processa_uf(nome_uf)
    
    pass